const express = require("express");
const cors = require("cors");
const app = express();
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger-output.json");

const authRoutes = require("./src/Routes/AuthRoutes");
const customerRoutes = require("./src/Routes/CustomerRoutes");
const adminRoutes = require("./src/Routes/AdminRoutes");
const productRoutes = require("./src/Routes/ProductRoutes");
const categoryRoutes = require("./src/Routes/CategoryRoutes");
const cartRoutes = require("./src/Routes/CartRoutes");
const paymentRoutes = require("./src/Routes/PaymentRoutes");
const orderItemRoutes = require("./src/Routes/OrderItemRoute");

var options = {
  customCss:
    ".swagger-ui .opblock .opblock-summary-path-description-wrapper { align-items: center; display: flex; flex-wrap: wrap; gap: 0 10px; padding: 0 10px; width: 100%; } .swagger-ui button.opblock-control-arrow {cursor: pointer;border: none; background: none;}",
  customCssUrl: "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/4.6.2/swagger-ui.css",
  customSiteTitle: "E-Commerce App API Documentation",
};

app.use(express.json());

app.use(cors());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile, options));

app.use("/", authRoutes);
app.use("/", customerRoutes);
app.use("/", adminRoutes);
app.use("/", productRoutes);
app.use("/", categoryRoutes);
app.use("/", cartRoutes);
app.use("/", paymentRoutes);
app.use("/", orderItemRoutes);

app.listen(4555, () => {
  console.log("berjalan di port 4555");
});
