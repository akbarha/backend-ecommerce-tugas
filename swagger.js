const swaggerAutogen = require("swagger-autogen")({ openapi: "3.0.0" });

const doc = {
  info: {
    title: "E-Commerce pakaian API",
    description: "Description",
  },
  host: process.env.HOST,
  components: {
    securitySchemes: {
      bearerAuth: {
        type: "http",
        scheme: "bearer",
      },
    },
    schemas: {
      authSchema: {
        $username: "username",
        $email: "email@example.com",
        $password: "password",
        $address: "address",
        $phone_number: "phone_number",
      },
      profileSchema: {
        $username: "username",
        $email: "email@example.com",
        $address: "address",
        $phone_number: "phone_number",
      },
      adminSchema: {
        $username: "username",
        $password: "password",
      },
      changePasswordSchema: {
        $currentPassword: "currentPassword",
        $newPassword: "newPassword",
      },
      productSchema: {
        $description: "description",
        $product_name: "product_name",
        $price: 0,
        $stock: 0,
        $image_url: "http://example.com",
        $category_id: 0,
      },
      categorySchema: {
        $name: "name",
      },
      cartSchema: {
        $quantity: 0,
        $product_id: 0,
      },
      paymentSchema: {
        $payment_method: "method",
        $amount: 0,
        $bukti_bayar: "http://image_url.com",
        $order_id: 0,
      },
    },
  },
};

const outputFile = "./swagger-output.json";
const routes = ["./index.js"];

/* NOTE: If you are using the express Router, you must pass in the 'routes' only the 
root file where the route starts, such as index.js, app.js, routes.js, etc ... */

swaggerAutogen(outputFile, routes, doc).then(() => {
  require("./index.js"); // Your project's root file
});
