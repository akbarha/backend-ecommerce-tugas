const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const getProfile = async (req, res) => {
  // #swagger.tags = ['Customer']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */

  if (req.user.role !== "customer") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    // Cari pengguna berdasarkan ID pengguna
    const userId = req.user.id;
    const user = await prisma.customer.findUnique({
      where: {
        id: parseInt(req.user.id),
      },
    });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Mengembalikan informasi profil pengguna
    res.status(200).json({ user });
  } catch (error) {
    console.error("Error fetching user profile:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

const GetAllCustomer = async (req, res) => {
  // #swagger.tags = ['Customer']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    let customers = await prisma.customer.findMany();
    res.json(customers);
  } catch (err) {
    console.error("Error fetching user profile:", err);
    res.status(500).json({ err });
  }
};

const updateProfile = async (req, res) => {
  // #swagger.tags = ['Customer']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/profileSchema"
                }  
            }
        }
      } 
    */

  let { id } = req.params; // Ambil userId dari parameter URL
  const { username, email, address, phone_number } = req.body; // Ambil data yang akan diperbarui dari body permintaan

  try {
    // Perbarui profil pengguna
    const updatedUser = await prisma.customer.update({
      where: {
        id: parseInt(id),
      },
      data: {
        username,
        email,
        address,
        phone_number,
      },
    });

    // Jika pengguna tidak ditemukan, kembalikan respons dengan status kode 404
    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    // Kembalikan informasi profil pengguna yang telah diperbarui
    res.status(200).json({ message: "User profile updated successfully", user: updatedUser });
  } catch (error) {
    console.error("Error updating user profile:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

const deleteCustomer = async (req, res) => {
  // #swagger.tags = ['Customer']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */

  let { id } = req.params; // Ambil userId dari parameter URL

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    // Hapus pengguna dari database
    const deletedUser = await prisma.customer.deleteMany({
      where: {
        id: parseInt(id),
      },
    });

    // Jika pengguna tidak ditemukan, kembalikan respons dengan status kode 404
    if (!deletedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    // Kembalikan respons sukses dengan status kode 200
    res.status(200).json({ message: "User deleted successfully" });
  } catch (error) {
    console.error("Error deleting user:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

module.exports = { getProfile, updateProfile, deleteCustomer, GetAllCustomer };
