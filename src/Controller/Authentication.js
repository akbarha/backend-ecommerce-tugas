const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");

const register = async (req, res) => {
  // #swagger.tags = ['Auth']
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/authSchema"
                }  
            }
        }
      } 
    */
  const { username, email, password, address, phone_number } = req.body;

  // Check if the user already exists
  const existingUser = await prisma.customer.findFirst({
    where: {
      OR: [{ username }, { email }],
    },
  });

  if (existingUser) {
    return res.status(409).json({ message: "User already exists" });
  }

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, 10);

  try {
    // Create the new user
    const newUser = await prisma.customer.create({
      data: {
        username,
        email,
        password: hashedPassword,
        address, // tambahkan address
        phone_number, // tambahkan phone_number
      },
    });

    res.status(201).json({ message: "User registered successfully", user: newUser });
  } catch (error) {
    console.error("Error registering user:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

const login = async (req, res) => {
  // #swagger.tags = ['Auth']
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/adminSchema"
                }  
            }
        }
      } 
    */
  const { username, password } = req.body;
  const customer = await prisma.customer.findFirst({
    where: {
      username,
    },
  });
  if (!customer || !(await bcrypt.compare(password, customer.password))) {
    return res.status(401).json({ message: "Invalid credentials" });
  }
  const payload = { username: customer.username, id: customer.id, role: "customer" };
  const accessToken = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "24h" });
  res.json({ accessToken });
};

const changePassword = async (req, res) => {
  // #swagger.tags = ['Auth']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/changePasswordSchema"
                }  
            }
        }
      } 
    */
  const { currentPassword, newPassword } = req.body;

  try {
    const user = await prisma.customer.findUnique({
      where: {
        username: req.user.username,
      },
    });

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    const passwordValid = await bcrypt.compare(currentPassword, user.password);
    if (!passwordValid) {
      return res.status(401).json({ message: "Invalid current password" });
    }

    const hashedNewPassword = await bcrypt.hash(newPassword, 10);

    await prisma.customer.update({
      where: {
        id: user.id,
      },
      data: {
        password: hashedNewPassword,
      },
    });

    res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.error("Error changing password:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

module.exports = { register, login, changePassword };
