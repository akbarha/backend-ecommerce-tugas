const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const createPayment = async (req, res) => {
  // #swagger.tags = ['Payment']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/paymentSchema"
                }  
            }
        }
      } 
    */
  const { payment_method, amount, bukti_bayar, order_id } = req.body;
  if (payment_method !== undefined && amount !== undefined && bukti_bayar !== undefined && order_id !== undefined) {
    if (req.user.role !== "customer") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    try {
      const payment = await prisma.payment.create({
        data: {
          payment_method,
          amount,
          bukti_bayar,
          customer_id: req.user.id,
          order_id,
        },
      });
      await prisma.order.update({
        where: { id: order_id },
        data: { status: "PAID" },
      });
      res.json({ payment, message: "Payment created successfully" });
    } catch (err) {
      res.status(500).json({ message: err });
    }
  } else {
    res.json({ message: "payment_method, amount, bukti_bayar, customer_id, order_id is required" });
  }
};

const getPayment = async (req, res) => {
  // #swagger.tags = ['Payment']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  try {
    const payment = await prisma.payment.findMany({
      include: {
        customer: true,
        order: {
          include: {
            OrderItem: {
              include: {
                product: {
                  include: {
                    category: true,
                  },
                },
              },
            },
          },
        },
      },
    });
    const formatterPayment = payment.map((item) => {
      return {
        id: item.id,
        payment_method: item.payment_method,
        amount: item.amount,
        bukti_bayar: item.bukti_bayar,
        customer: item.customer.username,
        order_date: item.order.order_date,
        order_item: item.order.OrderItem.map((item) => {
          return {
            quantity: item.quantity,
            price: item.price,
            product_name: item.product.product_name,
            image_url: item.product.image_url,
            category: item.product.category.name,
          };
        }),
      };
    });
    res.json(formatterPayment);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

const getPaymentById = async (req, res) => {
  // #swagger.tags = ['Payment']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  const { id } = req.params;
  try {
    const payment = await prisma.payment.findUnique({
      where: {
        id: Number(id),
      },
      include: {
        customer: true,
        order: {
          include: {
            OrderItem: {
              include: {
                product: {
                  include: {
                    category: true,
                  },
                },
              },
            },
          },
        },
      },
    });
    const formatterPayment = {
      id: payment.id,
      payment_method: payment.payment_method,
      amount: payment.amount,
      bukti_bayar: payment.bukti_bayar,
      customer: payment.customer.username,
      order_date: payment.order.order_date,
      order_item: payment.order.OrderItem.map((item) => {
        return {
          quantity: item.quantity,
          price: item.price,
          product_name: item.product.product_name,
          image_url: item.product.image_url,
          category: item.product.category.name,
        };
      }),
    };
    res.json(formatterPayment);
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: err });
  }
};

const updatePayment = async (req, res) => {
  // #swagger.tags = ['Payment']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/paymentSchema"
                }  
            }
        }
      } 
    */
  const { id } = req.params;
  const { payment_method, amount, bukti_bayar, order_id } = req.body;
  if (payment_method !== undefined && amount !== undefined && bukti_bayar !== undefined && order_id !== undefined) {
    if (req.user.role !== "customer") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    try {
      const payment = await prisma.payment.update({
        where: {
          id: Number(id),
        },
        data: {
          payment_method,
          amount,
          bukti_bayar,
          customer_id: req.user.id,
          order_id,
        },
      });
      await prisma.order.update({
        where: { id: parseInt(order_id) },
        data: { status: "PAID" },
      });
      res.json({ payment, message: "Payment updated successfully" });
    } catch (err) {
      res.status(500).json({ message: err });
    }
  } else {
    res.json({ message: "payment_method, amount, bukti_bayar, customer_id, order_id is required" });
  }
};

const deletePayment = async (req, res) => {
  // #swagger.tags = ['Payment']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  const { id } = req.params;
  try {
    const payment = await prisma.payment.deleteMany({
      where: {
        id: Number(id),
      },
    });
    res.json({ payment, message: "Payment deleted successfully" });
  } catch (err) {
    if (err.code === "P2025") {
      res.status(404).json({ message: "data not found" });
    } else {
      res.status(500).json({ message: err });
    }
  }
};

module.exports = {
  createPayment,
  getPayment,
  getPaymentById,
  updatePayment,
  deletePayment,
};
