const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getAllProduct = async (req, res) => {
  // #swagger.tags = ['Product']
  try {
    const allProduct = await prisma.product.findMany({
      include: {
        category: true,
      },
    });

    const formattedAllProduct = allProduct.map((item) => {
      return {
        id: item.id,
        product_name: item.product_name,
        description: item.description,
        price: item.price,
        stock: item.stock,
        image_url: item.image_url,
        category: item.category.name,
        category_id: item.category.id,
      };
    });
    res.json(formattedAllProduct);
  } catch (err) {
    console.error(err);
  }
};

const getProductByCategoryId = async (req, res) => {
  // #swagger.tags = ['Product']
  const { categoryId } = req.params;

  try {
    const category = await prisma.category.findMany({
      where: {
        id: parseInt(categoryId),
      },
      include: {
        product: true,
      },
    });

    const formattedProducts = category
      .map((category) => {
        return category.product.map((product) => {
          return {
            id: product.id,
            product_name: product.product_name,
            description: product.description,
            price: product.price,
            stock: product.stock,
            image_url: product.image_url,
            category_id: category.id,
            category: category.name,
          };
        });
      })
      .flat();

    res.json(formattedProducts);
  } catch (error) {
    console.log(error);
  }
};

const getProductById = async (req, res) => {
  // #swagger.tags = ['Product']
  const { id } = req.params;
  try {
    const product = await prisma.product.findFirst({
      where: {
        id: parseInt(id),
      },
      include: {
        category: true,
      },
    });

    const formattedProduct = {
      id: product.id,
      product_name: product.product_name,
      description: product.description,
      price: product.price,
      stock: product.stock,
      image_url: product.image_url,
      category: product.category.name,
      category_id: product.category.id,
    };
    res.json(formattedProduct);
  } catch (err) {
    console.error(err);
  }
};

const createProduct = async (req, res) => {
  // #swagger.tags = ['Product']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/productSchema"
                }  
            }
        }
      } 
    */
  const { description, product_name, price, stock, image_url, category_id } = req.body;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    const newProduct = await prisma.product.create({
      data: {
        product_name: product_name,
        description: description,
        price: price,
        stock: stock,
        image_url: image_url,
        category_id: category_id,
      },
    });

    res.json({ info: "success", data: newProduct });
  } catch (err) {
    console.error(err);
    res.json(err);
  }
};

const updateProduct = async (req, res) => {
  // #swagger.tags = ['Product']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/productSchema"
                }  
            }
        }
      } 
    */
  const { product_name, description, price, stock, image_url, category_id } = req.body;
  const { id } = req.params;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    // check ada
    const checkExist = await prisma.product.findFirst({
      where: {
        id: parseInt(id),
      },
    });

    if (!checkExist) {
      return res.status(404).json({ info: "Product Data Not Found" });
    }

    const updatedProduct = await prisma.product.update({
      where: {
        id: parseInt(id),
      },
      data: {
        product_name: product_name,
        description: description,
        price: price,
        stock: stock,
        image_url: image_url,
        category_id: category_id,
      },
    });

    res.json({ info: "Update Sukses", data: updatedProduct });
  } catch (err) {
    console.error(err);
    res.json(err);
  }
};

const deleteProduct = async (req, res) => {
  // #swagger.tags = ['Product']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  const { id } = req.params;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    // check jika beneran ada productnya
    const checkExist = await prisma.product.findFirst({
      where: {
        id: parseInt(id),
      },
    });

    if (!checkExist) {
      return res.status(404).json({ info: "Data tidak ditemukan" });
    }

    const deletedProduct = await prisma.product.deleteMany({
      where: {
        id: parseInt(id),
      },
    });

    res.json({ info: "Produk Berhasil Dihapus", deletedProduct: deletedProduct });
  } catch (err) {
    console.error(err);
    res.json(err);
  }
};

module.exports = {
  getAllProduct,
  getProductById,
  getProductByCategoryId,
  createProduct,
  updateProduct,
  deleteProduct,
};
