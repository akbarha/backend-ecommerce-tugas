const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const createCategory = async (req, res) => {
  // #swagger.tags = ['Category']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/categorySchema"
                }  
            }
        }
      } 
    */
  const { name } = req.body;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  if (name !== undefined) {
    try {
      const category = await prisma.category.create({
        data: {
          name,
        },
      });
      res.json({ category, message: "Category created successfully" });
    } catch (err) {
      res.status(500).json({ message: err });
    }
  } else {
    res.json({ message: "Name is required" });
  }
};

const GetCategory = async (req, res) => {
  // #swagger.tags = ['Category']
  try {
    const category = await prisma.category.findMany();
    res.json(category);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

const GetCategoryById = async (req, res) => {
  // #swagger.tags = ['Category']
  const { id } = req.params;

  try {
    const category = await prisma.category.findUnique({
      where: {
        id: Number(id),
      },
    });
    res.json(category);
  } catch (err) {
    res.status(500).json({ message: err });
  }
};

const UpdateCategory = async (req, res) => {
  // #swagger.tags = ['Category']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/categorySchema"
                }  
            }
        }
      } 
    */
  const { id } = req.params;
  const { name } = req.body;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  if (name !== undefined) {
    try {
      const category = await prisma.category.update({
        where: {
          id: Number(id),
        },
        data: {
          name,
        },
      });
      res.json({ category, message: "Category updated successfully" });
    } catch (err) {
      if (err.code === "P2025") {
        res.status(404).json({ message: "data not found" });
      } else {
        res.status(500).json({ message: err });
      }
    }
  } else {
    res.status(400).json({ message: "Name is required" });
  }
};

const DeleteCategory = async (req, res) => {
  // #swagger.tags = ['Category']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  const { id } = req.params;

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    const category = await prisma.category.deleteMany({
      where: {
        id: Number(id),
      },
    });
    res.json({ category, message: "Category deleted successfully" });
  } catch (err) {
    if (err.code === "P2025") {
      res.status(404).json({ message: "data not found" });
    } else {
      res.status(500).json({ message: err });
    }
  }
};

module.exports = {
  createCategory,
  GetCategory,
  GetCategoryById,
  UpdateCategory,
  DeleteCategory,
};
