const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();
// const axios = require('axios')

const OrderItemController = {
  createOrderItem: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */

    if (req.user.role !== "customer") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    try {
      // 1. Temukan Cart milik customer
      const customerCart = await prisma.cart.findMany({
        where: {
          customer_id: req.user.id,
        },
        include: {
          product: true,
        },
      });

      const formattedCustomerCart = customerCart.map((item) => {
        return {
          product: item.product.product_name,
          product_id: item.product_id,
          price: item.product.price,
          quantity: item.quantity,
          customer_id: item.customer_id,
        };
      });

      // itung dulu total price dari looping cart milik customer
      let total_price = 0;

      formattedCustomerCart.forEach((cart) => {
        total_price = total_price + cart.price * cart.quantity;
      });

      // console.log(total_price);

      // buat order
      const order = await prisma.order.create({
        data: {
          total_price: total_price,
          customer_id: req.user.id,
          // status: "UNPAID",
        },
      });
      console.log("order berhasil dibuat!");

      // console.log(customerCart)
      // console.log("bikin order item");

      // buat order item untuk setiap item di dalam cart, ini akan dimasukan ke dalam order
      formattedCustomerCart.forEach(async (cart) => {
        // console.log(cart.price);
        await prisma.orderItem.create({
          data: {
            quantity: cart.quantity,
            price: cart.price,
            product_id: cart.product_id,
            order_id: order.id,
          },
        });
      });

      console.log("order item berhasil dibuat");
      const orderitems = await prisma.orderItem.findMany({
        where: {
          order_id: order.id,
        },
      });

      await prisma.cart.deleteMany({
        where: {
          customer_id: req.user.id,
        },
      });

      res.json({ order: order, orderitems: orderitems });
    } catch (err) {
      console.error(err);
      res.status(500).json({ info: err });
    }
  },

  getAllOrder: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    try {
      const order = await prisma.order.findMany({
        include: {
          customer: true,
          Payment: true,
          OrderItem: {
            include: {
              product: {
                include: {
                  category: true,
                },
              },
            },
          },
        },
      });

      const formattedOrder = order.map((item) => {
        return {
          id: item.id,
          customer: item.customer.username,
          order_date: item.order_date,
          total_price: item.total_price,
          status: item.status,
          bukti_bayar: item.Payment.length === 1 ? item.Payment[0].bukti_bayar : null,
          order_item: item.OrderItem.map((item) => {
            return {
              id: item.id,
              quantity: item.quantity,
              price: item.price,
              product_name: item.product.product_name,
              image_url: item.product.image_url,
              category: item.product.category.name,
            };
          }),
        };
      });

      res.json(formattedOrder);
    } catch (err) {
      console.error(err);
      res.json(err);
    }
  },

  getOrderById: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    let { id } = req.params;
    try {
      const order = await prisma.order.findUnique({
        where: {
          id: Number(id),
        },
        include: {
          customer: true,
          Payment: true,
          OrderItem: {
            include: {
              product: {
                include: {
                  category: true,
                },
              },
            },
          },
        },
      });
      const formattedOrder = {
        id: order.id,
        customer: order.customer.username,
        email: order.customer.email,
        phone_number: order.customer.phone_number,
        order_date: order.order_date,
        total_price: order.total_price,
        status: order.status,
        bukti_bayar: order.Payment.length === 1 ? order.Payment[0].bukti_bayar : null,
        order_item: order.OrderItem.map((item) => {
          return {
            id: item.id,
            quantity: item.quantity,
            price: item.price,
            product_name: item.product.product_name,
            image_url: item.product.image_url,
            category: item.product.category.name,
          };
        }),
      };
      res.json(formattedOrder);
    } catch (err) {
      console.error(err);
      res.json(err);
    }
  },

  getOrderByCustomer: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    try {
      const order = await prisma.order.findMany({
        where: {
          customer_id: req.user.id,
        },
        include: {
          customer: true,
          OrderItem: {
            include: {
              product: {
                include: {
                  category: true,
                },
              },
            },
          },
        },
      });

      const formattedOrder = order.map((item) => {
        return {
          id: item.id,
          customer: item.customer.username,
          email: item.email,
          phone_number: item.customer.phone_number,
          order_date: item.order_date,
          total_price: item.total_price,
          status: item.status,
          order_item: item.OrderItem.map((item) => {
            return {
              id: item.id,
              quantity: item.quantity,
              price: item.price,
              product_name: item.product.product_name,
              image_url: item.product.image_url,
              category: item.product.category.name,
            };
          }),
        };
      });
      res.json(formattedOrder);
    } catch (err) {
      console.error(err);
      res.json(err);
    }
  },

  updateOrderConfirmed: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    const { id } = req.params;

    if (req.user.role !== "admin") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    try {
      await prisma.order.update({
        where: { id: parseInt(id) },
        data: { status: "CONFIRMED" },
      });

      res.json({ message: `The order has been confirmed.` });
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },

  updateOrderRejected: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    const { id } = req.params;

    if (req.user.role !== "admin") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    try {
      await prisma.order.update({
        where: { id: parseInt(id) },
        data: { status: "REJECTED" },
      });

      res.json({ message: "The order has been rejected." });
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },

  updateOrderCanceled: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    const { id } = req.params;

    if (req.user.role !== "customer") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }
    console.log(id);
    try {
      await prisma.order.update({
        where: { id: parseInt(id) },
        data: { status: "CANCELED" },
      });

      res.json({ message: "Your order was canceled successfully." });
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },

  deleteOrderAndOrderItem: async (req, res) => {
    // #swagger.tags = ['Order']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    let { id } = req.params;

    // console.log(id)

    await prisma.orderItem.deleteMany({
      where: {
        order_id: parseInt(id),
      },
    });

    await prisma.order.deleteMany({
      where: {
        id: parseInt(id),
      },
    });

    res.json("successfully deleted order and order items");
  },
};

module.exports = OrderItemController;
