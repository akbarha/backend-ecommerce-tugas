const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const getAllCart = async (req, res) => {
  // #swagger.tags = ['Cart']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */

  if (req.user.role !== "admin") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    const allCartItems = await prisma.cart.findMany();

    if (!allCartItems) {
      return res.status(500).json({ message: "No cart data could be found" });
    }

    return res.status(200).json(allCartItems);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: err.message });
  }
};

const getCartByCustomerId = async (req, res) => {
  // #swagger.tags = ['Cart']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  if (req.user.role !== "customer") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }
  try {
    const cart = await prisma.cart.findMany({
      where: {
        customer_id: parseInt(req.user.id),
      },
      include: {
        product: true,
      },
    });

    const formattedCart = cart.map((item) => {
      return {
        id: item.id,
        product_id: item.product_id,
        product: item.product.product_name,
        stock: item.product.stock,
        price: item.product.price,
        quantity: item.quantity,
        customer_id: item.customer_id,
        image_url:item.product.image_url
      };
    });

    if (!cart) {
      return res.status(500).json({ message: "No data could be found" });
    }

    return res.status(200).json(formattedCart);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: err.message });
  }
};

const createCart = async (req, res) => {
  // #swagger.tags = ['Cart']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/cartSchema"
                }  
            }
        }
      } 
    */
  const { quantity, product_id } = req.body;

  if (req.user.role !== "customer") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    const existingCartItem = await prisma.cart.findFirst({
      where: {
        product_id: Number(product_id),
        customer_id: req.user.id,
      },
    });

    if (existingCartItem) {
      return res.status(400).json({ message: "Product already exists in the cart." });
    }

    const newCart = await prisma.cart.create({
      data: {
        quantity: Number(quantity),
        product_id: Number(product_id),
        customer_id: req.user.id,
      },
    });

    return res.status(200).json(newCart);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: err.message });
  }
};

const updateCart = async (req, res) => {
  // #swagger.tags = ['Cart']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/cartSchema"
                }  
            }
        }
      } 
    */
  const { id } = req.params;
  const { quantity, product_id } = req.body;

  if (req.user.role !== "customer") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    const updatedCart = await prisma.cart.update({
      where: {
        id: Number(id),
      },
      data: {
        quantity: Number(quantity),
        product_id: Number(product_id),
        customer_id: req.user.id,
      },
    });

    return res.status(200).json(updatedCart);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: err.message });
  }
};

const deleteCart = async (req, res) => {
  // #swagger.tags = ['Cart']
  /* #swagger.security = [{
            "bearerAuth": []
    }] */
  const { id } = req.params;

  if (req.user.role !== "customer") {
    return res.status(500).json({ message: "You don't have permission to perform this action." });
  }

  try {
    await prisma.cart.deleteMany({
      where: {
        id: Number(id),
      },
    });

    return res.status(200).json({ message: "Successfully deleted cart" });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: err.message });
  }
};

module.exports = {
  getAllCart,
  getCartByCustomerId,
  createCart,
  updateCart,
  deleteCart,
};
