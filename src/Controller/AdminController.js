const { PrismaClient } = require("@prisma/client");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const prisma = new PrismaClient();

const AdminController = {
  createAdmin: async (req, res) => {
    // #swagger.tags = ['Admin']
    /* #swagger.security = [{
            "bearerAuth": []
    }] */
    /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/adminSchema"
                }  
            }
        }
      } 
    */
    const { username, password } = req.body;

    if (req.user.role !== "admin") {
      return res.status(500).json({ message: "You don't have permission to perform this action." });
    }

    const admin = await prisma.admin.findFirst({
      where: { username },
    });
    if (admin) {
      return res.status(409).json({ info: "Admin already exists" });
    }
    const hashedPassword = await bcrypt.hash(password, 10);

    await prisma.admin.create({
      data: {
        username,
        password: hashedPassword,
      },
    });
    res.status(201).json({ info: "Admin created successfully" });
  },

  loginAdmin: async (req, res) => {
    // #swagger.tags = ['Admin']
    /*  #swagger.requestBody = {
        required: true,
        content: {
            "application/json": {
                schema: {
                    $ref: "#/components/schemas/adminSchema"
                }  
            }
        }
      } 
    */
    const { username, password } = req.body;
    const admin = await prisma.admin.findUnique({
      where: { username: username },
    });
    if (!admin || !(await bcrypt.compare(password, admin.password))) {
      return res.status(401).json({ info: "Username or password invalid" });
    }
    const payload = { username: admin.username, id: admin.id, role: "admin" };
    const accessToken = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: "24h" });
    res.json({ accessToken });
  },
};

module.exports = AdminController;
