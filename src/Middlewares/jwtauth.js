const jwt = require("jsonwebtoken");

const authenticateJWTAdmin = (req, res, next) => {
  const authorization = req.header("Authorization");
  if (!authorization) return res.sendStatus(401);

  const token = authorization.split(" ")[1];
  if (!token) return res.sendStatus(401);

  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;

    // res.json({ user });
    next();
    // Menambahkan informasi pengguna ke dalam respon
  });
};

const authenticateJWT = (req, res, next) => {
  const authorization = req.header("Authorization").split(" ");
  const token = authorization[1];
  if (!token) return res.sendStatus(401);
  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};

module.exports = {
  authenticateJWT,
  authenticateJWTAdmin,
};
