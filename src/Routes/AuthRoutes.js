const express = require("express");
const router = express.Router();

const { register, login, changePassword } = require("../Controller/Authentication");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.post("/register", register);
router.post("/loginuser", login);
router.patch("/changePassword", authenticateJWT, changePassword);

module.exports = router;
