const express = require("express");
const router = express.Router();

const { getAllCart, getCartByCustomerId, createCart, updateCart, deleteCart } = require("../Controller/CartController");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.get("/cart/admin", authenticateJWT, getAllCart);
router.get("/cart", authenticateJWT, getCartByCustomerId);
router.post("/cart/create", authenticateJWT, createCart);
router.patch("/cart/:id", authenticateJWT, updateCart);
router.delete("/cart/:id", authenticateJWT, deleteCart);

module.exports = router;
