const express = require("express");
const router = express.Router();

const OrderItemController = require("../Controller/OrderItemController");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.get("/order", authenticateJWT, OrderItemController.getAllOrder);
router.get("/order/customer", authenticateJWT, OrderItemController.getOrderByCustomer);
router.get("/order/:id", authenticateJWT, OrderItemController.getOrderById);
router.post("/order", authenticateJWT, OrderItemController.createOrderItem);
router.patch("/order/:id/confirm", authenticateJWT, OrderItemController.updateOrderConfirmed);
router.patch("/order/:id/reject", authenticateJWT, OrderItemController.updateOrderRejected);
router.patch("/order/:id/cancel", authenticateJWT, OrderItemController.updateOrderCanceled);
router.delete("/order/:id", authenticateJWT, OrderItemController.deleteOrderAndOrderItem);

module.exports = router;
