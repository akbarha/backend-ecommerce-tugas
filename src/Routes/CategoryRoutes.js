const express = require("express");
const router = express.Router();

const { GetCategory, GetCategoryById, createCategory, UpdateCategory, DeleteCategory } = require("../Controller/CategoryController");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.get("/category", GetCategory);
router.get("/category/:id", GetCategoryById);
router.post("/category/create", authenticateJWT, createCategory);
router.patch("/category/:id", authenticateJWT, UpdateCategory);
router.delete("/category/:id", authenticateJWT, DeleteCategory);

module.exports = router;
