const express = require("express");
const router = express.Router();

const { authenticateJWT } = require("../Middlewares/jwtauth");
const { getProfile, updateProfile, deleteCustomer, GetAllCustomer } = require("../Controller/CustomerController");

router.get("/profilecustomer", authenticateJWT, getProfile);
router.get("/profile/", authenticateJWT, GetAllCustomer);
router.patch("/updateprofile/:id", authenticateJWT, updateProfile);
router.delete("/deleteuser/:id", authenticateJWT, deleteCustomer);

module.exports = router;
