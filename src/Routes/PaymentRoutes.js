const express = require("express");
const router = express.Router();

const { createPayment, getPayment, getPaymentById, updatePayment, deletePayment } = require("../Controller/PaymentController");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.get("/payment", authenticateJWT, getPayment);
router.get("/payment/:id", authenticateJWT, getPaymentById);
router.post("/payment/create", authenticateJWT, createPayment);
router.patch("/payment/:id", authenticateJWT, updatePayment);
router.delete("/payment/:id", authenticateJWT, deletePayment);

module.exports = router;
