const express = require("express");
const router = express.Router();

const { getAllProduct, getProductById, getProductByCategoryId, createProduct, updateProduct, deleteProduct } = require("../Controller/ProductController");
const { authenticateJWT } = require("../Middlewares/jwtauth");
router.get("/products/all", getAllProduct);
router.get("/product/:id", getProductById);
router.get("/product/:categoryId/category", getProductByCategoryId);
router.post("/product/create", authenticateJWT, createProduct);
router.patch("/product/:id", authenticateJWT, updateProduct);
router.delete("/product/:id", authenticateJWT, deleteProduct);

module.exports = router;
