const express = require("express");
const router = express.Router();

const AdminController = require("../Controller/AdminController");
const { authenticateJWT } = require("../Middlewares/jwtauth");

router.post("/admin/register", authenticateJWT, AdminController.createAdmin);
router.post("/admin/login", AdminController.loginAdmin);

module.exports = router;
