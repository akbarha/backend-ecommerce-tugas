-- CreateEnum
CREATE TYPE "Status" AS ENUM ('UNPAID', 'PAID', 'CONFIRMED', 'REJECTED', 'CANCELED');

-- AlterTable
ALTER TABLE "Order" ADD COLUMN     "status" "Status" NOT NULL DEFAULT 'UNPAID';
